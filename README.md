### Introduction

A minimalist starter config specifically for C++ development based on [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim).

Works *only* on the latest stable neovim release (0.8) and the nightly.

See [kickstart.nvim wiki](https://github.com/nvim-lua/kickstart.nvim/wiki) for additional tips, tricks, and recommended plugins.

### Installation
* Backup your previous configuration
* Install `lazygit`, `rg` and `fzf`
* Copy and paste `init.lua` from this repo into `$HOME/.config/nvim/init.lua`
* start neovim (`nvim`) and run `:PackerInstall`, ignore any error message about missing plugins, `:PackerInstall` will fix that shortly.
* restart neovim
