------------------------------------------------------
------------------------------------------------------
-- Package manager, plugin installation, base setup --
------------------------------------------------------
------------------------------------------------------

local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
local is_bootstrap = false
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  is_bootstrap = true
  vim.fn.execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
  vim.cmd [[packadd packer.nvim]]
end

-- TODO: lazygit with delta config like on Mac
-- TODO: update rainbow parentheses plugin
-- TODO: separate key for snippets vs completion, tab results in conflicts/mistakes too often
-- TODO: move the live_grep (<leader>sg) mapping to a local init.vim, so it can be set per project,
--       and add a grep mapping for Component as well
--       See ':help exrc' for how to do this
-- TODO: refactoring.nvim?
-- TODO: https://github.com/ray-x/lsp_signature.nvim   - to show signature as I'm writing a function
--       call even if I have issues with the default way to do it
-- TODO: debugprint.nvim
-- TODO: HighStr.nvim
-- TODO: https://github.com/ziontee113/syntax-tree-surfer  - for e.g. swapping arguments, list items,
--       functions - can  use C-j, C-k and so on - which I previously used for dumb moving
-- TODO: venn.nvim
-- TODO: look at lualine extensions
-- TODO: aerial.nvim (togglable sidebar with a jumpable list of all LSP symbols)
-- TODO: configure LSP formatting somehow and have a shortcut to format file
-- TODO (HARD): nvim-gdb or vimspector or nvim-dap, whichever is easier to get to work
-- TODO multi-cursors with vim-visual-multi  (consider a project to rewrite in lua)
-- Project: C++ snippets for luasnip as a plugin, with configurable coding style
--              (but not doc generation, recommend neogen for that)
--              - should also do stuff like partial format string generation for printf and similar
--                for std::format
--              - different variant for start of line and middle of line (std::vector<T> name; vs std::vector<T>)
-- Mini-project: Luasnip: highlight next jump points - can i do that as a pr?
-- TODO: Plugins potentially useful in future:
-- TODO: new stuff from kickstart.nvim, maybe e.g. updated (bugfixed if possible) yank highlight
--
-- Editing:
-- - Structural search and replace - very useful for large-scale semi-automated refactoring
--   https://github.com/cshuaimin/ssr.nvim/        
-- Movement:
-- - Faster motions (sneak-like): 
--   https://github.com/ggandor/leap.nvim

-- stylua: ignore start
require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'                                                                           -- Package manager
  -------- tree-sitter --------
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}                                             
  use { 'nvim-treesitter/nvim-treesitter-textobjects', after = { 'nvim-treesitter' } }                   -- Additional textobjects for treesitter
  use 'nvim-treesitter/nvim-treesitter-context'                                                          -- show first line of current function when scrolled beyond its start
  -------- telescope --------
  use { 'nvim-telescope/telescope.nvim', branch = '0.1.x', requires = { 'nvim-lua/plenary.nvim' } }      -- Fuzzy Finder (files, lsp, etc)
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', cond = vim.fn.executable "make" == 1 } -- Fuzzy Finder Algorithm which requires local dependencies to be built. Only load if `make` is available
  -------- LSP and autocomplete --------
  use 'neovim/nvim-lspconfig'                                                                            -- Collection of configurations for built-in LSP client
  use { 'hrsh7th/nvim-cmp', requires = { 'hrsh7th/cmp-nvim-lsp' } }                                      -- Autocompletion
  use 'hrsh7th/cmp-buffer'                                                                               -- 'buffer' source for nvim-cmp                                              
  use 'hrsh7th/cmp-path'                                                                                 -- 'path' source for nvim-cmp                                                
  use 'hrsh7th/cmp-cmdline'                                                                              -- 'cmdline' source for nvim-cmp                                             
  use 'hrsh7th/cmp-calc'                                                                                 -- 'calc' source for math expression results                                 
  use { 'smjonas/inc-rename.nvim', config = function() require("inc_rename").setup() end, }              -- LSP rename with immediate visual feedback
  use { 'https://git.sr.ht/~whynothugo/lsp_lines.nvim',                                                  -- show LSP diagnostics on virtual text lines, giving an IDE-like display with arrows and such
    config = function() require("lsp_lines").setup() end,
  }
  -------- snippets --------
  use { 'L3MON4D3/LuaSnip', requires = { 'saadparwaiz1/cmp_luasnip' } }                                  -- Snippet Engine and Snippet Expansion
  use 'honza/vim-snippets'                                                                               -- snipmate snippets for luasnip                                                                       
  -------- operators --------
  use 'numToStr/Comment.nvim'                                                                            -- "gc" to comment visual regions/lines
  use { 'kylechui/nvim-surround', tag = "*", config = function() require("nvim-surround").setup() end }  -- surround text objects/operators
  use 'junegunn/vim-easy-align'                                                                          -- alignment operator (gaip and such)
  -------- non-operator editing features --------
  use { 'windwp/nvim-autopairs', config = function() require("nvim-autopairs").setup {} end }            -- auto-close pairs like '{' and '"'
  use { 'AckslD/nvim-neoclip.lua',                                                                       -- remember all past yanks (up to 2048)
    requires = { 'nvim-telescope/telescope.nvim' },
    config = function()
      require('neoclip').setup{
        history = 2048,      
        content_spec_column = true,
        default_register = {'"', '+', '*'},
      }
    end,
  }
  use { 'danymat/neogen',                                                                                -- documentation comment generation
    config = function()
        require('neogen').setup {}
    end,
    requires = "nvim-treesitter/nvim-treesitter",
    tag = "*"
  }
  -------- modes/tools/windows --------
  use 'mbbill/undotree'                                                                                  -- undo tree browser
  use 'dhruvasagar/vim-zoom'                                                                             -- tmux-style window zoom
  use { 'akinsho/toggleterm.nvim', tag = '*', config = function() require("toggleterm").setup() end }    -- togglable terminal, usable for mappings to launch commands (e.g. lazygit)
  use { 'elihunter173/dirbuf.nvim',                                                                      -- file manager similar to vinegar
    config = function() require("dirbuf").setup { sort_order = "directories_first" } end
  }
  use { 'rmagatti/goto-preview',                                                                         -- open 'go to destination' in a floating window
    config = function()
      require('goto-preview').setup {
        height = 24; -- Height of the floating window
      }
    end
  }
  use { 'folke/which-key.nvim',                                                                          -- auto-popup help info bottom bar listing possible next keys and what they will do
    config = function()
      require("which-key").setup {
        operators = { gc = "Comments",
                      ga = "Align" },
        layout = {
          height  = { min = 5, max = 25 },  -- min and max height of the columns
          width   = { min = 20, max = 80 }, -- min and max width of the columns
          spacing = 3,                      -- spacing between columns
          align   = "center",               -- align columns left, center or right
        },
        window = {
          border = "single",        -- none, single, double, shadow
          position = "bottom",      -- bottom, top
          margin = { 1, 0, 1, 0 },  -- extra window margin [top, right, bottom, left]
          padding = { 0, 2, 0, 2 }, -- extra window padding [top, right, bottom, left]
          winblend = 10
        },
      }
    end
  }
  -------- colorschemes and display --------
  use 'mjlbach/onedark.nvim'                                                                             -- color scheme: Theme inspired by Atom
  use 'ellisonleao/gruvbox.nvim'                                                                         -- color scheme
  use 'rebelot/kanagawa.nvim'                                                                            -- color scheme
  use 'nvim-lualine/lualine.nvim'                                                                        -- Fancier statusline
  use 'p00f/nvim-ts-rainbow'                                                                             -- rainbow parentheses
  use 'RRethy/vim-illuminate'                                                                            -- highlight all instances of word under cursor (intentionally dumb with no LSP support to match '*')
  use { 'm-demare/hlargs.nvim',                                                                          -- highlight function parameters and all their usages
    requires = { 'nvim-treesitter/nvim-treesitter' },
    config = function() 
      require('hlargs').setup({
        color="#ffa072"
      })
    end
  }
  use { 'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' } }                              -- Add git related info in the signs columns and popups
  -------- configuration/adaptability/persistence --------
  use 'tpope/vim-sleuth'                                                                                 -- Detect tabstop and shiftwidth automatically
  use 'Shatur/neovim-session-manager'                                                                    -- sessions/persistence
  if is_bootstrap then
    require('packer').sync()
  end
end)
-- stylua: ignore end

-- When we are bootstrapping a configuration, it doesn't
-- make sense to execute the rest of the init.lua.
--
-- You'll need to restart nvim, and then it will work.
if is_bootstrap then
  print '=================================='
  print '    Plugins are being installed'
  print '    Wait until Packer completes,'
  print '       then restart nvim'
  print '=================================='
  return
end

-- Automatically source and re-compile packer whenever you save this init.lua
local packer_group = vim.api.nvim_create_augroup('Packer', { clear = true })
vim.api.nvim_create_autocmd('BufWritePost', {
  command = 'source <afile> | PackerCompile',
  group = packer_group,
  pattern = vim.fn.expand '$MYVIMRC',
})


---------------------------------------
---------------------------------------
-- Global options, see `:help vim.o` --
---------------------------------------
---------------------------------------

-------- system environment --------
-- Sync working directory with current file
vim.o.autochdir = true
-- yank to system clipboard using the `+` register: requires `xclip` (sudo apt install xclip)
vim.cmd("set clipboard+=unnamedplus")
-- Don't generate swap files
vim.o.swapfile = false

-------- text editing behavior --------
-- Autoformatting - keep text up to this width
vim.o.textwidth   = 100
-- Enable the spellchecker
vim.o.spelllang   = "en"
vim.o.spell       = true
-- Enable mouse mode
vim.o.mouse       = 'a'
-- Wrapped line will match indent of the previous line
vim.o.breakindent = true
-- No wrapping by default
vim.o.wrap        = false
-- Save undo history
vim.o.undofile    = true
vim.o.undolevels  = 8192
vim.o.undoreload  = 16384
-- Indentation
vim.o.tabstop    = 4
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.smartindent = true
vim.o.expandtab = true
-- when adding a line, copy exactly the indent characters used on previous line.
vim.o.copyindent = true

-------- search/replace --------
-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase  = true
-- Real-time preview of a search/replace
vim.o.inccommand = "nosplit"

-------- autocompletion --------
-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-------- nvim TUI --------
-- Confirmation dialogs with multiple choices instead of just preventing us from e.g. quitting a file
vim.o.confirm    = true
-- Saner split behavior
vim.o.splitright = true
vim.o.splitbelow = true
-- scroll so we always have at least 4 lines above/below the cursor
vim.o.scrolloff = 4
-- like scrolloff, but horizontal
vim.o.sidescrolloff = 8
-- current window will be at least 25 lines as long as it's possible
vim.o.winheight = 25
-- Allow moving cursor beyond file contents
vim.o.virtualedit = "all"
-- Faster responsiveness
vim.o.updatetime = 200
-- Combined with 'which-key.nvim', this quickly shows mapping help info and allows us to complete a
-- mapping. If we remove 'which-key.nvim', this must be removed, otherwise mappings will be
-- impossible to use  (by default, this affects the 'wait time' for next key in a mapping)
vim.o.timeoutlen = 100
vim.cmd [[ 
  augroup InsertTimeoutlen
    autocmd InsertEnter * set timeoutlen=500
    autocmd InsertLeave * set timeoutlen=100
  augroup end
]]
-- needed to render git signs
vim.wo.signcolumn = 'yes'
-- Highlight searched item
vim.o.hlsearch = true
vim.o.colorcolumn = "100,120"
-- don't show mode in command line, since we already see it in the statusline
vim.o.showmode = false
-- don't use space for command-line when it's not active
vim.o.cmdheight=0
---- folding ----
vim.o.foldenable = false --folds open by default
vim.o.foldmethod = "expr"
-- TODO: figure out if this works at all and consider returning to treesitter folding (probably need to re-enable treesitter indent) or something else (maybe a plugin)
-- vim.opt.foldexpr="nvim_treesitter#foldexpr()"
vim.o.foldexpr   = "GetPotionFold(v:lnum)"
-- open all folds by default, but we can close them if needed
vim.o.foldlevel  = 99
vim.o.foldtext   = "CustomFoldText()"
-- force-regenerate folds on buffer open, even if it's open from telescope as opposed to ':e'
vim.cmd [[
  augroup FoldRebuild
    autocmd!
    autocmd BufReadPost * set foldmethod=expr
  augroup end
]]
-- TODO: rewrite in lua
vim.cmd [[
function! GetPotionFold(lnum)
  if getline(a:lnum) =~? '\v^\s*$'
    return '-1'
  endif

  let this_indent = IndentLevel(a:lnum)
  let next_indent = IndentLevel(NextNonBlankLine(a:lnum))

  if next_indent == this_indent
    return this_indent
  elseif next_indent < this_indent
    return this_indent
  elseif next_indent > this_indent
    return '>' . next_indent
  endif
endfunction

function! IndentLevel(lnum)
    return indent(a:lnum) / &shiftwidth
endfunction

function! NextNonBlankLine(lnum)
  let numlines = line('$')
  let current = a:lnum + 1

  while current <= numlines
    if getline(current) =~? '\v\S'
      return current
    endif

    let current += 1
  endwhile

  return -2
endfunction

function! CustomFoldText()
  " get first non-blank line
  let fs = v:foldstart

  while getline(fs) =~ '^\s*$' | let fs = nextnonblank(fs + 1)
  endwhile

  if fs > v:foldend
      let line = getline(v:foldstart)
  else
      let line = substitute(getline(fs), '\t', repeat(' ', &tabstop), 'g')
  endif

  let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
  let foldSize = 1 + v:foldend - v:foldstart
  let foldSizeStr = " " . foldSize . " lines "
  let foldLevelStr = repeat("+--", v:foldlevel)
  let expansionString = repeat("_", w - strwidth(foldSizeStr.line.foldLevelStr))
  return line . expansionString . foldSizeStr . foldLevelStr
endfunction
]]
---- colors ----
vim.o.termguicolors = true
vim.o.winblend      = 10 -- transparent float windows (0 fully opague, 100 fully transparent)
-- this will affect all the hl-groups where the redefined colors are used
local my_colors = {
    sumiInk1 = "#07070A",
}
local my_overrides = {
    WinSeparator = { fg = "#4D4D78", bg = "NONE" },
    VertSplit    = { fg = "#4D4D78", bg = "NONE" },
}
require('kanagawa').setup({
    undercurl            = true,  -- enable undercurls
    commentStyle         = { italic = true },
    functionStyle        = {},
    keywordStyle         = { italic = true},
    statementStyle       = { bold = true },
    typeStyle            = {},
    variablebuiltinStyle = { italic = true},
    specialReturn        = true,   -- special highlight for the return keyword
    specialException     = true,   -- special highlight for exception handling keywords
    transparent          = false,  -- do not set background color
    dimInactive          = false,  -- dim inactive window `:h hl-NormalNC`
    globalStatus         = false,  -- adjust window separators highlight for laststatus=3
    terminalColors       = true,   -- define vim.g.terminal_color_{0,17}
    colors               = my_colors,
    overrides            = my_overrides,
    theme                = "default" -- Load "default" theme or the experimental "light" theme
})
vim.cmd("colorscheme kanagawa")

-------- global mappings --------
-- <space> as the leader key
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
-- see `:help vim.keymap.set()`
vim.g.mapleader      = ' '
vim.g.maplocalleader = ' '
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true, desc = 'disable default \'<Space>\' mapping to free it as leader' })
-- movements & actions
vim.keymap.set('n', 'H', "^",                    { silent = true, desc = 'start of line' })
vim.keymap.set('n', 'L', "$",                    { silent = true, desc = 'end of line' })
vim.keymap.set('n', '<C-c>', ":nohlsearch<CR>",  { silent = true, desc = 'clear search highlight' })
-- remaps for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true, desc = 'up, not skipping wrapped lines' })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true, desc = 'down, not skipping wrapped lines' })
vim.keymap.set('x', '?', '<Esc>/\\%V',                { desc = 'search in selection' })
-- strain reduction
vim.keymap.set('i', 'jj', "<Esc>",          { silent = true, desc = 'exit insert mode' })
vim.keymap.set('t', 'jj', "<C-\\><C-n>",    { silent = true, desc = 'exit terminal' })
vim.keymap.set('c', 'jj', "<Esc>",          { silent = true, desc = 'exit command-line' })
vim.keymap.set('n', '\'', ":",              { desc = 'command-line without holding <Shift>'})
vim.keymap.set('v', '\'', ":'<,'>",         { desc = 'command-line without holding <Shift>'})
-- window management
vim.keymap.set('n', '<C-w>o', "<Nop>",      { remap = true,  silent = true, desc = 'disable :only (closes all other windows)' })
vim.keymap.set('n', '<C-w>o', "<Nop>",      { remap = false, silent = true, desc = 'disable :only (closes all other windows)' })
vim.keymap.set('n', '<PageDown>', "<C-w>",  { silent = true, desc = 'PgDn instead of <C-w> for window management' })
vim.keymap.set('n', '<F10>', "<C-w>=",      { silent = true, desc = 'Equalize window sizes' })
vim.keymap.set('n', '<Leader>=', "6<C-w>+", { silent = true, desc = 'Increase window size' })
vim.keymap.set('n', '<Leader>-', "6<C-w>-", { silent = true, desc = 'Decrease window size' })
-- config
vim.keymap.set('n', '<Leader>C', ":e $MYVIMRC<cr>", { silent = true, desc = 'init.lua' })
-- folding
vim.keymap.set('n', '<CR>', "zo", { silent = true, desc = 'open fold' })
vim.keymap.set('n', '<BS>', "zc", { silent = true, desc = 'close fold' })
-- repeat last macro
vim.keymap.set('n', '<F2>', "@@", { silent = true, desc = 'repeat last macro' })
-- diagnostic keymaps
vim.keymap.set('n', '[d',        vim.diagnostic.goto_prev,  { desc = 'next [d]iagnostic' })
vim.keymap.set('n', ']d',        vim.diagnostic.goto_next,  { desc = 'previous [d]iagnostic' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'diagnostic d[e]tails' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = '[q]iucklist diagnostics' })


--------------------
--------------------
-- plugin configs --
--------------------
--------------------

-------- lualine --------
-- see `:help lualine.txt`
--- return function that can format the component accordingly ----
local function trunc(trunc_len)
  return function(str)
    if str:len() > trunc_len  then
       return str:sub(1, trunc_len) .. ('…')
    else 
        return str
    end
  end
end
local function window()
  return vim.api.nvim_win_get_number(0)
end
require('lualine').setup {
  options = {
    icons_enabled        = false,
    theme                = 'onedark',
    component_separators = '┊',--'│', ,-- '╱',
    section_separators   = '',
  },
  sections = {
    lualine_a = {window, { 'mode', fmt = function(str) return str:sub(1,1) end }},
    lualine_b = {{'branch', fmt = trunc(20)}, 'diff', 'diagnostics'},
    lualine_c = {},
    lualine_x = {'filetype', 'filesize'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {window},
    lualine_b = {},
    lualine_c = {{'filename', newfile_status = true, path=3, shorting_target=80}},
    lualine_x = {'filetype', 'filesize'},
    lualine_y = {'progress'},
    lualine_z = {'location'},
  },
}

-------- Comment.nvim --------
require('Comment').setup()

-------- gitsigns --------
-- See `:help gitsigns.txt`
require('gitsigns').setup {
  signs = {
    add          = { text = '+' },
    change       = { text = '~' },
    delete       = { text = '_' },
    topdelete    = { text = '‾' },
    changedelete = { text = '~' },
  },
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns

    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    -- next/previous change (hunk)
    map('n', ']c', function()
      if vim.wo.diff then return ']c' end
      vim.schedule(function() gs.next_hunk() end)
      return '<Ignore>'
    end, { expr=true, desc = "next [c]hange" })
    map('n', '[c', function()
      if vim.wo.diff then return '[c' end
      vim.schedule(function() gs.prev_hunk() end)
      return '<Ignore>'
    end, { expr=true, desc = "previous [c]hange" })

    -- actions
    map({'n', 'v'}, '<leader>hs', ':Gitsigns stage_hunk<CR>',               { desc = '[hs]tage hunk'      })
    map({'n', 'v'}, '<leader>hr', ':Gitsigns reset_hunk<CR>',               { desc = '[hr]eset hunk'      })
    map('n',        '<leader>hS', gs.stage_buffer,                          { desc = '[hS]tage buffer'    })
    map('n',        '<leader>hu', gs.undo_stage_hunk,                       { desc = '[hu]ndo stage hunk' })
    map('n',        '<leader>hR', gs.reset_buffer,                          { desc = '[hR]eset buffer'    })
    map('n',        '<leader>hp', gs.preview_hunk,                          { desc = '[hp]review hunk'    })
    map('n',        '<leader>hb', function() gs.blame_line{ full=true} end, { desc = '[hb]lame line'      })
    map('n',        '<leader>tb', gs.toggle_current_line_blame,             { desc = '[t]toggle [B]lame line'})
    map('n',        '<leader>hd', gs.diffthis,                              { desc = '[hd]iff file'       })
    map('n',        '<leader>hD', function() gs.diffthis('~') end,          { desc = '[hD]iff ???'        })
    map('n',        '<leader>td', gs.toggle_deleted,                        { desc = '[t]oggle [d]eleted' })

    -- text object
    map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>', { desc = "inner hunk" })
  end
}

-------- telescope.nvim ---------
-- see `:help telescope` and `:help telescope.setup()`
require('telescope').setup {
  defaults = { mappings = { i = { }, }, },
  extensions = { }, 
}
-- enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')

---- telescope mappings ----
-- See `:help telescope.builtin`
vim.keymap.set('n', '<leader>?',       require('telescope.builtin').oldfiles, { desc = 'Find recently opened files' })
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers,  { desc = 'Find existing buffers' })
vim.keymap.set('n', '<leader>/',       function()
    -- pass additional configuration to telescope to change theme, layout, etc.
    require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
      winblend  = 10,
      previewer = false,
    })
  end, { desc = 'Fuzzily search in current buffer' })
vim.keymap.set('n', '<leader>F', function()
    local opts = {}
    local ok = pcall(require"telescope.builtin".git_files, opts)
    if not ok then require"telescope.builtin".find_files(opts) end
  end, { desc = 'git [F]iles' })
vim.keymap.set('n', '<leader>sg', function() require('telescope.builtin').live_grep({cwd="/home/kiith-sa/Sync/v01d"}) end, { desc = '[s]earch by [g]rep' })
vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files,      { desc = '[s]earch [f]iles' })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags,       { desc = '[s]earch [h]elp' })
vim.keymap.set('n', '<leader>sm', require('telescope.builtin').man_pages,       { desc = '[s]earch [m]an pages' })
vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string,     { desc = '[s]earch current [w]ord' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics,     { desc = '[s]earch [d]iagnostics' })
vim.keymap.set('n', '<leader>sH', require('telescope.builtin').command_history, { desc = '[s]earch [h]istory' })
vim.keymap.set('n', '<leader>sv', require('telescope.builtin').vim_options,     { desc = '[s]earch [v]im options' })
vim.keymap.set('n', '<leader>st', require('telescope.builtin').treesitter,      { desc = '[s]earch [t]reesitter' })
-- git-related telescope mappings
vim.keymap.set('n', '<leader>sc', require('telescope.builtin').git_bcommits,    { desc = '[s]earch [c]ommits touching this file' })
vim.keymap.set('n', '<leader>sx', require('telescope.builtin').git_commits,     { desc = '[s]earch all [x]commits' })
vim.keymap.set('n', '<leader>sb', require('telescope.builtin').git_branches,    { desc = '[s]earch [b]ranches' })
vim.keymap.set('n', '<leader>ss', require('telescope.builtin').git_status,      { desc = '[s]earch git [s]tatus' })
vim.keymap.set('n', '<leader>s;', require('telescope.builtin').git_stash,       { desc = '[s]earch git [;]stash' })

-------- nvim-treesitter --------
-- See `:help nvim-treesitter`
require('nvim-treesitter.configs').setup {
  -- Add languages to be installed here that you want installed for treesitter
  ensure_installed = { 'bash', 'c', 'cmake', 'cpp', 'css', 'cuda', 'go', 'http', 'html', 'latex', 'lua', 'json', 'make', 'markdown', 'markdown_inline', 'python', 'regex', 'rst', 'rust', 'toml', 'typescript', 'vim', 'yaml' },
  highlight        = { enable = true },
  indent           = { enable = false },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection    = '<leader>n',
      node_incremental  = 'n',
      scope_incremental = '<leader>n',
      node_decremental  = 'N',
    },
  },
  textobjects = {
    select = {
      enable    = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
    },
  },
}

-------- LSP settings (not a plugin, but requires plugins to work) --------

-- this function runs when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  -- function to more easily define mappings specific for LSP related items.
  -- It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end
    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  ---- lsp mappings ----
  nmap('<leader>rn', ":IncRename ",                                                    '[r]e[n]ame')
  nmap('<leader>ca', vim.lsp.buf.code_action,                                          '[c]ode [a]ction')
  nmap('gD',         vim.lsp.buf.definition,                                           '[g]oto [D]efinition')
  nmap('gd',         function() require('goto-preview').goto_preview_definition() end, '[g]oto preview [d]efinition')
  nmap('gq',         function() require('goto-preview').close_all_win() end,           '[g]oto preview [q]uit')
  nmap('gi',         vim.lsp.buf.implementation,                                       '[g]oto [i]mplementation')
  nmap('gr',         require('telescope.builtin').lsp_references,                      '[g]et [r]eferences')
  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols,                '[d]ocument [s]ymbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols,       '[w]orkspace [s]ymbols')
  -- see `:help K` for why this keymap                                                 
  nmap('K',          vim.lsp.buf.hover,                                                'hover documentation')
  nmap('<C-k>',      vim.lsp.buf.signature_help,                                       'signature documentation')
  nmap('<F4>',       ":ClangdSwitchSourceHeader<cr>",                                  'switch source/header')
  -- lesser used LSP functionality
  nmap('<leader>gd', vim.lsp.buf.declaration,                                          '[g]oto [D]eclaration')
  nmap('<leader>D',  vim.lsp.buf.type_definition,                                      'type [D]efinition')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder,                                 '[w]orkspace [a]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder,                              '[w]orkspace [r]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[w]orkspace [l]ist Folders')

  -- create command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    if vim.lsp.buf.format then
      vim.lsp.buf.format()
    elseif vim.lsp.buf.formatting then
      vim.lsp.buf.formatting()
    end
  end, { desc = 'Format current buffer with LSP' })
end

-- nvim-cmp supports additional completion capabilities
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

-- set up above mappings for these LSP servers
local servers = { 'clangd', 'rust_analyzer', 'pyright' }
for _, lsp in ipairs(servers) do
  require('lspconfig')[lsp].setup {
    on_attach    = on_attach,
    capabilities = capabilities,
  }
end

---- nvim-cmp and luasnip setup ----
local cmp     = require('cmp')
local luasnip = require('luasnip')
luasnip.config.set_config({
  store_selection_keys = '<Tab>',
})

--require("luasnip.loaders.from_vscode").lazy_load()
require("luasnip.loaders.from_snipmate").lazy_load()
local cmp_tab = function(fallback)
  -- snippet takes priority over completion
  if luasnip.expand_or_jumpable() then
    luasnip.expand_or_jump()
  elseif cmp.visible() then
    cmp.select_next_item()
  else
    fallback()
  end
end

local cmp_shift_tab = function(fallback)
  if luasnip.jumpable(-1) then
    luasnip.jump(-1)
  elseif cmp.visible() then
    cmp.select_prev_item()
  else
    fallback()
  end
end

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-d>']     = cmp.mapping.scroll_docs(-4),
    ['<C-f>']     = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ["<C-e>"]     = cmp.mapping.abort(),
    ['<Tab>']     = cmp.mapping(cmp_tab, { 'i', 's' }),
    ['<S-Tab>']   = cmp.mapping(cmp_shift_tab, { 'i', 's' }),
    ['<CR>']      = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select   = true,
    },
  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
      { name = 'buffer',
        option = {
        get_bufnrs = function()
          local bufs = {}
          for _, win in ipairs(vim.api.nvim_list_wins()) do
            bufs[vim.api.nvim_win_get_buf(win)] = true
          end
          return vim.tbl_keys(bufs)
        end
        }
      },
      -- { name = 'path' },
      { name = 'calc' },
  },
}

-- allow search autocompletion to use buffer words
cmp.setup.cmdline('/', {
  sources = {
    { name = 'buffer' }
  }
})
-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
  sources = cmp.config.sources(
    { { name = 'path'    } },
    { { name = 'cmdline' } }
  )
})

-------- nvim-ts-rainbow --------
require("nvim-treesitter.configs").setup {
  highlight = {},
  rainbow = {
    enable         = true,
    extended_mode  = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
  }
}

-------- vim-easy-align --------
-- Start interactive EasyAlign in visual mode (e.g. vipga)
vim.keymap.set('x', 'ga', "<Plug>(EasyAlign)", { silent = true, desc = 'interactive EasyAlign (visual)' })
-- Start interactive EasyAlign for a motion/text object (e.g. gaip)
vim.keymap.set('n', 'ga', "<Plug>(EasyAlign)", { silent = true, desc = 'interactive EasyAlign (motion)' })

-------- vim-zoom --------
vim.keymap.set('n', '<leader>z', "<Plug>(zoom-toggle)", { silent = true, desc = 'zoom' })

-------- undotree --------
-- F5 to show the undotrree
vim.keymap.set('n', '<F5>', ":UndotreeToggle<cr>", { silent = true, desc = 'undotree' })
-- don't take up too much horizontal space
vim.g.undotree_SplitWidth=25
-- focus the undotree when displayed
vim.g.undotree_SetFocusWhenToggle=1
-- save horizontal space
vim.g.undotree_ShortIndicators=1

-------- vim-illuminate --------
require('illuminate').configure({
    -- providers: provider used to get references in the buffer, ordered by priority
    -- NOTE: intentionally dumb to match '*'
    providers = { 'regex', },
    -- delay: delay in milliseconds
    delay = 100,
    -- filetype_overrides: filetype specific overrides.
    -- The keys are strings to represent the filetype while the values are tables that
    -- supports the same keys passed to .configure except for filetypes_denylist and filetypes_allowlist
    filetype_overrides = {},
    -- filetypes_denylist: filetypes to not illuminate, this overrides filetypes_allowlist
    filetypes_denylist = {
        'dirvish',
        'fugitive',
    },
    -- filetypes_allowlist: filetypes to illuminate, this is overriden by filetypes_denylist
    filetypes_allowlist = {},
    -- modes_denylist: modes to not illuminate, this overrides modes_allowlist
    modes_denylist = {},
    -- modes_allowlist: modes to illuminate, this is overriden by modes_denylist
    modes_allowlist = {},
    -- providers_regex_syntax_denylist: syntax to not illuminate, this overrides providers_regex_syntax_allowlist
    -- Only applies to the 'regex' provider
    -- Use :echom synIDattr(synIDtrans(synID(line('.'), col('.'), 1)), 'name')
    providers_regex_syntax_denylist = {},
    -- providers_regex_syntax_allowlist: syntax to illuminate, this is overriden by providers_regex_syntax_denylist
    -- Only applies to the 'regex' provider
    -- Use :echom synIDattr(synIDtrans(synID(line('.'), col('.'), 1)), 'name')
    providers_regex_syntax_allowlist = {},
    -- under_cursor: whether or not to illuminate under the cursor
    under_cursor = true,
    -- max_file_lines: max number of lines in a file to illuminate
    max_file_lines = nil,
})

-------- neovim-session-manager --------
local Path = require('plenary.path')
require('session_manager').setup({
  sessions_dir               = Path:new(vim.fn.stdpath('data'), 'sessions'),            -- directory where the session files will be saved.
  path_replacer              = '__',                                                    -- character to which the path separator will be replaced for session files.
  colon_replacer             = '++',                                                    -- character to which the colon symbol will be replaced for session files.
  autoload_mode              = require('session_manager.config').AutoloadMode.Disabled, -- Define what to do when Neovim is started without arguments. Possible values: Disabled, CurrentDir, LastSession
  autosave_last_session      = true,                                                    -- Automatically save last session on exit and on session switch.
  autosave_ignore_not_normal = true,                                                    -- Plugin will not save a session when no buffers are opened, or all of them aren't writable or listed.
  autosave_ignore_filetypes  = { 'gitcommit', },                                        -- All buffers of these file types will be closed before the session is saved.
  autosave_only_in_session   = false,                                                   -- Always autosaves session. If true, only autosaves after a session is active.
  max_path_length            = 80,                                                      -- Shorten the display path if length exceeds this threshold. Use 0 if don't want to shorten the path at all.
})

-------- lsp_lines --------
-- Disable virtual_text since it's redundant due to lsp_lines.
vim.diagnostic.config({
  virtual_text = false,
})

-------- toggleterm --------
local Terminal  = require('toggleterm.terminal').Terminal
local lazygit = Terminal:new({ cmd = 'lazygit', direction = 'float', hidden = true })
function _lazygit_toggle() lazygit:toggle() end
vim.keymap.set('n', '<F6>', _lazygit_toggle, { silent = true, desc = 'lazygit' })

-------- neoclip --------
require('telescope').load_extension('neoclip')
-- search all yanks
vim.keymap.set('n', '<leader>p', "<cmd>Telescope neoclip extra=plus<CR>" , { desc = '[s]earch yanks/paste' })

-------- neogen  --------
require('neogen').setup({ snippet_engine = 'luasnip' })
vim.keymap.set('n', '<leader>gf', require('neogen').generate, { desc = '[g]enerate [f]unction docs' })
vim.keymap.set('n', '<leader>gc', function() require('neogen').generate({ type="class" }) end, { desc = '[g]enerate [c]lass docs' })

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
